from flask import Flask, request, render_template, redirect, url_for, session, g, redirect
from sql import safeCreateTable, searchTableColumnEntry, addEntry, updateEntry, countRows, searchTable, safeCreateTableFK
from hashAndSalt import passwordHash, quickHash
from emailValidation import validateEmail
from requests import get
from QRCode import encodeQR, decodeQR
# from aes256 import encrypt, decrypt
from bs4 import BeautifulSoup
# from OpenSSL import SSL
from PIL import Image

from flask_kvsession import KVSessionExtension
from simplekv.memory.redisstore import RedisStore

import pycountry, datetime, requests, random, string, time, csv, sys, os, re



app = Flask(__name__)
app.secret_key = (os.urandom(32))
# context = SSL.Context(SSL.proto)

# Let's Encrypt. is a free certificate authority
# A tutorial can be found here THIS IS THE RIGHT LINK SCROLL DOWN https://blog.miguelgrinberg.com/post/running-your-flask-application-over-https

# print("Global IP: http://" + str(get('https://api.ipify.org').text)+":5001/")
# urlPrefix = "http://" + str(get('https://api.ipify.org').text)+":5001"
urlPrefix = "http://localhost:5001"

databaseName = "database/main.db"


faqs = [
    [
        "GENERAL",
        [
            [
                "Is everyone eligible to participate in your raffles?",
                "Our company is catered solely to the regulations of the United Kingdom and therefore we only allow participants from the UK, above the age of 18."
            ],
            [
                "How do I enter one of your ongoing raffles?",
                "There are two ways in which you can do this. You can either enter via our website and redeem a ticket from your wallet for the raffle or you can submit a free postal entry. For more information regarding the wallet system or postal entries, please scroll down and read the designated entries for both items."
            ],
            [
                "How do I know if I’m the winner of a raffle?",
                "If your ticket number is the winner, we will get in touch with you via the contact details you have provided on your account. We will reach out to you on the following working day from when the draw has concluded, to congratulate you. Every single winner is displayed publicly on our website and on our social media. So, as long as you keep your details up to date and you’re following our social media then we’ll get your prize to you - no sweat!	                                                                              Please be aware, we will make several efforts to contact winners but if we can’t get in touch with a winner after 14 days, we reserve the right to conduct a fresh draw for a new winner."
            ],
            [
                "How long does each raffle typically last?",
                "This varies for the different types of raffles we offer; each raffle listing will clearly state the duration in the description. There are also prominent countdown timers on the quick view displays of the raffles, throughout the website. However, the raffle may end prematurely if all the nominated slots sell out quicker than the allocated time frame."
            ],
            [
                "What happens if all of the allocated tickets aren’t sold?",
                "The majority of raffles don’t have a minimum/reserve set and therefore even if we only sell a handful of tickets, a winner is still chosen. "
            ],
            [
                "Can I purchase more than one ticket per raffle?",
                "We have no issues with people purchasing more than 1 ticket to boost their odds but there’s a maximum ticket limit that you can’t go over. This limit is usually pretty low because we want our raffles to be as fair as possible. There is no universal number, this limit will vary from raffle to raffle."
            ],
            [
                "How can I see the raffles I’m currently entered into?",
                "You can keep track of the ongoing raffles you are participating in by logging into our website and visiting the Member Zone"
            ],
            [
                "Can I pull out of a raffle?",
                "Once you have chosen to participate in a particular raffle, you cannot retract your entry. All entries placed are final."
            ],
            [
                "Will products from previous raffles ever appear on the site again?",
                "This is entirely based upon the popularity for said product and if our members want the chance to win it again. We have a request an item form (link on the homepage), we will conduct polls for those requests to see which will be raffled in future. The power is in your hands!"
            ],
            [
                "Are your products confirmed as authentic?",
                "All of the items you see listed on our site are authentic and are sourced from respected consignment shops/retailers. We do not use stockx or any other mainstream service. We have been in the product procurement space for some time now and are connected with some of the best aftermarket suppliers."
            ],
            [
                "Do you ever do discounts or free ticket giveaways?",
                "We certainly do! Make sure to follow our Instagram @rafflesauce to be notified about the discount codes/freebies, they will also be limited to first come – first serve."
            ]
        ]
    ],
    [
        "SECURITY",
        [
            [
                "How do we store your passwords?",
                "Passwords - We hash and salt your passwords. This means we perform a non reversable operation to convert your password to a 64 character string of nonsence that will be the same every time. So if you loose your password not even RaffleSauce's tech team can tell you what it was. You'll have to reset it."                
            ],
            [
                "How do we store your other details?",
                "Everything else sensitive is stored with Military Grade Encryption also known as AES 256-bit. This means, without the encryption key it would take Japan's 'Fugaku' SuperComputer (The fastest in the world at the time of writing this) 6118961 trillion trillion trillion trillion years to find just 1 persons details, and thats not a typo."
            ]
        ]
    ],
    [
        "PRIZE INFO",
        [
            [
                "How will I receive my item if I win?",
                "If you are the winner of one of our raffles, we will either deliver your item in person or we will send it via insured delivery either UPS or Royal Mail."
            ],
            [
                "What happens if the winner doesn’t claim the prize?",
                "We will make several efforts to contact winners but if we can’t get in touch with a winner after 14 days, we reserve the right to conduct a fresh draw for a new winner."
            ],
            [
                "Are all of your prizes worth the same value?",
                "No, we have such a wide variety of prizes on offer all with differing valuations. We offer raffles for lots of brands with different pricing tiers, the value of each item is clearly marked on the raffles."
            ],
            [
                "Can members suggest new items for future raffles?",
                "Yes of course! In fact, we encourage this as it helps us to discover new products and expand into different areas of fashion. If you are interested in doing so, please fill out the form here to request an item"
            ]
        ]
    ]
]


TandC = [
    [
        "SECURITY",
        [
            [
                "How do we store your passwords?",
                "Passwords - We hash and salt your passwords. This means we perform a non reversable operation to convert your password to a 64 character string of nonsence that will be the same every time. So if you loose your password not even RaffleSauce's tech team can tell you what it was. You'll have to reset it."                
            ],
            [
                "How do we store your other details?",
                "Everything else sensitive is stored with Military Grade Encryption also known as AES 256-bit. This means, without the encryption key it would take Japan's 'Fugaku' SuperComputer (The fastest in the world at the time of writing this) 6118961 trillion trillion trillion trillion years to find just 1 persons details, and thats not a typo."
            ]
        ]
    ],
    [
        "PRIZE INFO",
        [
            [
                "How will I receive my item if I win?",
                "If you are the winner of one of our raffles, we will either deliver your item in person or we will send it via insured delivery either UPS or Royal Mail."
            ],
            [
                "What happens if the winner doesn’t claim the prize?",
                "We will make several efforts to contact winners but if we can’t get in touch with a winner after 14 days, we reserve the right to conduct a fresh draw for a new winner."
            ],
            [
                "Are all of your prizes worth the same value?",
                "No, we have such a wide variety of prizes on offer all with differing valuations. We offer raffles for lots of brands with different pricing tiers, the value of each item is clearly marked on the raffles."
            ],
            [
                "Can members suggest new items for future raffles?",
                "Yes of course! In fact, we encourage this as it helps us to discover new products and expand into different areas of fashion. If you are interested in doing so, please fill out the form here to request an item"
            ]
        ]
    ]
]


posts = [
    {
        "author": "Jordan Low",
        "title": "Item1",
        "cost": "£50",
        "dropDate": "1598671200000",
        "ticketcost": "2"
    },
    {
        "author": "Theodore Lawrance",
        "title": "Item2",
        "cost": "£75",
        "dropDate": "1598688400000",
        "ticketcost": "3"
    },
    {
        "author": "John Smith",
        "title": "Item3",
        "cost": "£100",
        "dropDate": "1598885600000",
        "ticketcost": "2"
    },
    {
        "author": "James Robert",
        "title": "Item4",
        "cost": "£125",
        "dropDate": "1599692800000",
        "ticketcost": "4"
    }
]


def isAdmin():
    if g.user == "problemlow":
        return(True)
    with open('admins.csv', 'r') as f:
        reader = csv.reader(f)
        array = list(reader)
    for user in array[0]:
        if g.user == user:
            return(True)
    return(False)


def tixLen(number):
    length = 4
    number = str(number)
    while len(number) < length:
        number = "0" + number
    return(number)

winners = []
for itera in range(3):
    winners.append(["user" + str(itera), "product" + str(itera), "static/images/product-bg-img.png", urlPrefix+"/static/Social.svg", "#"+tixLen(itera)])


def excludeCheck(tickets):
    if g.user:
        print("exclude - g.user: " + g.user)
        try:
            if g.exclude:
                rows = ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", g.exclude]
            else:
                rows = list(searchTableColumnEntry(databaseName, "users", "username", g.user)[0])
            if rows[20] != "NULL":
                endEpoch = float(rows[20])
                print("exclude - endEpoch: " + str(endEpoch))
                print("exclude - time.time(): " + str(time.time()))
                if time.time() < endEpoch:
                    endDate = time.strftime('%d/%m/%Y at %H:%M:%S', time.localtime(endEpoch))
                    print("exclude - endDate: " + str(endDate))
                    message = "You chose to exclude yourself until " + str(endDate) + ". Our staff won't unblock your account before then"
                    return(True, render_template("message.html", tickets=tickets, title="Information", message=["Information", message]))
        except Exception as e:
            print("exclude - Failed: "+ str(e))
            print("\n\n\n")
            return(False, redirect(url_for("memberzone")))
    else:
        return(False, redirect(url_for("login")))
    return(False, redirect(url_for("memberzone")))
    


def removeSpace(string):
    string = list(string)
    returnString = ""
    for character in string:
        if character == " ":
            returnString += "_"
        else:
            returnString += character
    return(returnString)


def loginCheck():
    if g.user:
        try:
            tickets = int(list(searchTableColumnEntry(databaseName, "users", "username", g.user)[0])[15])
        except:
            tickets = g.tickets
            print("loginCheck - something went wrong fallback logic caught it")
    else:
        tickets = 0
    return(tickets)


@app.route("/message")
def message():
    tickets = loginCheck()
    message = "This is a success message"
    return(render_template("message.html", tickets=tickets, title="Success", message=["Success", message], animation=True))


@app.route("/")
@app.route("/home")
def home():
    tickets = loginCheck()
    slides = [["BAG", "RAFFLES", "/home", "/static/Bags.png", "/static/Banner.png"], ["SHOE", "RAFFLES", "/home", "/static/Shoes.png", "/static/Banner.png"], ["ACCESSORY", "RAFFLES", "/home", "/static/Accessories.png", "/static/Banner.png"]]
    slides.append(slides[0])
    rows = searchTable(databaseName, "items")
    posts = []
    # slides = []
    for post in rows:
        rowsItem = searchTableColumnEntry(databaseName, "items", "name", post[1])
        url = "raffle/" + fixString(post[1])
        rowsItem = list(rowsItem[0])
        location = rowsItem[7]
        imageLocations = []
        for filename in os.listdir(location):
            imageLocations.append("/" + location + removeSpace(filename))
        try:
            randimg = imageLocations[0]
        except:
            randimg = "static/NotFound.png"
        if time.time() > float(rowsItem[9]) + 86400 and float(rowsItem[9]) < time.time():
            # 14400
            posts.append({
                "author":"None",
                "title":post[1],
                "cost":post[8],
                "dropDate":post[2],
                "ticketcost":post[3],
                "pageURL":url,
                "image":randimg
                }
            )

    return render_template("home.html", tickets=tickets, posts=posts, title="Home", winners=winners, slides=slides)


@app.route("/commingsoon")
def commingSoon():
    delay = 86400
    tickets = loginCheck()
    slides = [["BAG", "RAFFLES", "/home", "/static/Bags.png", "/static/Banner.png"], ["SHOE", "RAFFLES", "/home", "/static/Shoes.png", "/static/Banner.png"], ["ACCESSORY", "RAFFLES", "/home", "/static/Accessories.png", "/static/Banner.png"]]
    slides.append(slides[0])
    rows = searchTable(databaseName, "items")
    posts = []
    for post in rows:
        rowsItem = searchTableColumnEntry(databaseName, "items", "name", post[1])
        url = "raffle/" + fixString(post[1])
        rowsItem = list(rowsItem[0])
        location = rowsItem[7]
        imageLocations = []
        for filename in os.listdir(location):
            imageLocations.append("/" + location + removeSpace(filename))
        try:
            randimg = imageLocations[1]
        except:
            randimg = "static/NotFound.png"
        if time.time() < float(rowsItem[9]) + delay:
            posts.append({
                "author":"None",
                "title":post[1],
                "cost":post[8],
                "dropDate":float(rowsItem[9]) + delay,
                "ticketcost":post[3],
                "pageURL":url,
                "image":randimg
                }
            )
    return render_template("commingsoon.html", tickets=tickets, posts=posts, title="Home", winners=winners, slides=slides, isAdmin=isAdmin())


@app.errorhandler(404)
def pageNotFound(e):
    tickets = loginCheck()
    return render_template("message.html", tickets=tickets, title="Page Not Found", message=["404 - Page Not Found", "This page does not exist\nError: " + str(e)])


@app.route("/404")
@app.route("/notfound")
def notFound():
    tickets = loginCheck()
    return render_template("message.html", tickets=tickets, title="Page Not Found", message=["404 - Page Not Found", "This page does not exist\nNo Errors"])


@app.route("/privacypolicy")
def privacypolicy():
    tickets = loginCheck()
    return notFound()


@app.route("/termsandconditions")
def termsandconditions():
    tickets = loginCheck()
    return notFound()


# @app.route("/raffle")
# def raffle():
    # tickets = loginCheck()
    # return redirect("404")


@app.route("/raffle/<itemName>")
def dynamicraffle(itemName):
    tickets = loginCheck()
    rowsItem = searchTableColumnEntry(databaseName, "items", "name", itemName)
    try:
        rowsItem = list(rowsItem[0])
        variants = ["Select " + rowsItem[5]]
        for variant in rowsItem[6].upper().split(","):
            variants.append(variant)
        location = rowsItem[7]
        imageLocations = []
        print("dynamicraffle - " + str(location))
        for filename in os.listdir(location):
            imageLocations.append("/" + location + filename)
        print("dynamicraffle - " + str(imageLocations))
        if not imageLocations[0]:
            imageLocations.append("/static/SauceBGD.png")
        if not imageLocations[1]: 
            imageLocations.append("/static/SauceBGD.png")
        entries = [["1 ENTRY", str(rowsItem[3]*1)+" TICKETS"]]
        for entry in range(9):
            entries.append([str(entry+2) + " ENTRIES", str(rowsItem[3]*(entry+2))+" TICKETS"])
        print(rowsItem[4])
        item = {
            "author": rowsItem[0],
            "name": rowsItem[1],
            "dropDate": rowsItem[2]*1000,
            "value": rowsItem[8],
            "sizes": variants,
            "maxEntries": entries,
            "ticketCost": rowsItem[3],
            "description": rowsItem[4],
            "entries": [],
            "image0": imageLocations[0],
            "image1": imageLocations[1]
        }
        print("\n\n\n")
        print("\n\n\n")
        return render_template("raffledrop.html", tickets=tickets, item=item, title=itemName, variants=variants)
    except Exception as e:
        return(pageNotFound(str(e)))


@app.route("/enterraffle/<itemName>", methods=["GET", "POST"])
def enterraffle(itemName):
    tickets = loginCheck()
    checker, excludeReturn = excludeCheck(tickets)
    if checker == True:
        return(excludeReturn)
    print("\n\n\n")
    print("enterraffle - request.referrer: " + str(request.referrer))
    try:
        itemName = itemName
    # rowsItem = searchTableColumnEntry(databaseName, "items", "name", itemName)
        print("enterraffle - itemName: " + itemName)
    except Exception as e:
        return(pageNotFound(str(e)))
    if request.method == "POST":
        print("enterraffle - Method == POST")
        print("enterraffle - g.user" + str(g.user))
        if g.user:
            size = request.form["sizes"]
            entries = request.form["tickets"]
            if size == "None" or entries == "None":
                return(redirect(request.referrer))
            if entries == "Postal Entry" and size != "None":
                print("\n\n\n\n\n\n")
                print("enterraffle - Postal Entry: " + str(entries))
                print("enterraffle - Postal Entry: " + str(entries))
                print("enterraffle - Postal Entry: " + str(entries))
                print("enterraffle - Postal Entry: " + str(entries))
                print("enterraffle - Postal Entry: " + str(entries))
                print("enterraffle - Postal Entry: " + str(entries))
                print("enterraffle - Postal Entry: " + str(entries))
                print("enterraffle - Postal Entry: " + str(entries))
                session["postalVote"] = itemName + "---" + size
                print("\n\n\n\n\n\n")
                return(redirect(url_for("postalentry")))
            try:
                entries = int(entries[0:2])
                print("enterraffle - try1")
            except Exception as e:
                try:
                    entries = int(entries[0])
                    print("enterraffle - try2")
                except Exception as e:
                    return(redirect(request.referrer))
                    # return(pageNotFound(str(e)))
            try:
                rowsItem = list(searchTableColumnEntry(databaseName, "items", "name", itemName)[0])
                rowsUser = list(searchTableColumnEntry(databaseName, "users", "username", g.user)[0])
                print("enterraffle - rowsItem: " + str(rowsItem))
            except Exception as e:
                return(pageNotFound(str(e)))
            totalTix = entries*int(rowsItem[3])
            enteredRaffles = rowsUser[21]
            if int(tickets)-totalTix >= 0:
                updateEntry(databaseName, "users", "username", g.user, "tickets", int(tickets)-totalTix)
                print(int(tickets)-totalTix)
                for entry in range(entries):
                    raffleEntries = ["NULL", rowsUser[0], 0, 0, 0, str(time.time()), size, rowsUser[1]]
                    addEntry(databaseName, itemName+"Entries", raffleEntries)
                    if enteredRaffles == "NULL" or enteredRaffles == "":
                        print("\n\n\nrows item[0]: " + str(rowsItem[0]) + "\n\n\n")
                        enteredRaffles = str(rowsItem[0]) + " -#- " + str(int(time.time()))
                    else:
                        enteredRaffles = str(rowsItem[0]) + " -#- " + str(int(time.time())) + ", " + enteredRaffles
                print(enteredRaffles)

                updateEntry(databaseName, "users", "username", g.user, "enteredraffles", enteredRaffles)
                tickets = int(tickets)-totalTix
                session["tickets"] = tickets
                if totalTix == 1:
                    message = "enteredRaffles " + itemName + "raffle once for 1 ticket ticket"
                elif totalTix > 1:
                    message = "enteredRaffles " + itemName + " raffle " + str(entries) + " times for " + str(totalTix) + " tickets"
                return render_template("message.html", tickets=tickets, title="Success", message=["Success", message], loggedIn=True)
            else:
                tixNeeded = totalTix-int(tickets)
                return(redirect("/checkout/" + str(tixNeeded)))
        else:
            return(redirect(url_for("login")))

        
        
        print("enterraffle - entries: " + str(entries))
        print("enterraffle - ticketsfrm: " + str(request.form["tickets"]))
        print("enterraffle - size: " + size)
                

    elif request.method == "GET":
        print("enterraffle - Method == GET")
    else:
        print("enterraffle - Method == NONE")
    print("\n\n\n")
    
    message = "Something went wrong. You have not been charged"
    return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])


@app.route("/raffledrop/<itemName>")
def raffledrop(itemName):
    tickets = loginCheck()
    rowsItem = searchTableColumnEntry(databaseName, "items", "name", itemName)
    rowsDrop = searchTable(databaseName, removeSpace(itemName)+"Entries")
    winner = ["No one", "Entered"]
    try:
        rowsItem = list(rowsItem[0])
        entered = list(rowsDrop)
        entries = []
        if len(entered) > 0:
            while len(entries) < 40:
                for x in range(len(entered)):
                    number = str(x+1)
                    if len(number) < 4:
                        number = "0"*(4-len(number))+number
                    entries.append([number, entered[x][7]])
            winner = entries[random.randint(0,len(entries))]
        location = rowsItem[7]
        imageLocations = []
        print("raffledrop - " + str(location))
        for filename in os.listdir(location):
            imageLocations.append("/" + location + filename)
        print("raffledrop - " + str(imageLocations))
        if not imageLocations[0]:
            imageLocations.append("/static/SauceBGD.png")
        if not imageLocations[1]:
            imageLocations.append("/static/SauceBGD.png")
        item = {
            "author": rowsItem[0],
            "name": rowsItem[1],
            "dropDate": rowsItem[2]*1000,
            "value": rowsItem[2],
            "description": rowsItem[4],
            "entries": entries,
            "image0": imageLocations[0],
            "image1": imageLocations[1],
            "winner": winner
        }
        print("\n\n\n")
        print("raffledrop - g.tickets: " + str(g.tickets))
        print("\n\n\n")
        return render_template("raffledrop.html", tickets=tickets, item=item, title=itemName)
    except Exception as e:
        return(pageNotFound(str(e)))


@app.route("/newdrop/<itemName>")
def newdrop(itemName):
    tickets = loginCheck()
    rowsItem = searchTableColumnEntry(databaseName, "items", "name", itemName)
    rowsDrop = searchTable(databaseName, removeSpace(itemName)+"Entries")
    winner = ["No one", "Entered"]
    try:
        rowsItem = list(rowsItem[0])
        entered = list(rowsDrop)
        entries = []
        if len(entered) > 0:
            while len(entries) < 40:
                for x in range(len(entered)):
                    number = str(x+1)
                    if len(number) < 4:
                        number = "0"*(4-len(number))+number
                    entries.append([number, entered[x][7]])
            winner = entries[random.randint(0,len(entries))]
        location = rowsItem[7]
        imageLocations = []
        print("newdrop - " + str(location))
        for filename in os.listdir(location):
            imageLocations.append("/" + location + filename)
        print("newdrop - " + str(imageLocations))
        if not imageLocations[0]:
            imageLocations.append("/static/SauceBGD.png")
        if not imageLocations[1]:
            imageLocations.append("/static/SauceBGD.png")
        item = {
            "author": rowsItem[0],
            "name": rowsItem[1],
            "dropDate": rowsItem[2]*1000,
            "value": rowsItem[2],
            "description": rowsItem[4],
            "entries": entries,
            "image0": imageLocations[0],
            "image1": imageLocations[1],
            "winner": winner
        }
        tickets=g.tickets
        print("\n\n\n")
        print("newdrop - g.tickets: " + str(g.tickets))
        print("\n\n\n")
        return render_template("raffles-one.html", tickets=tickets, item=item, title=itemName)
    except Exception as e:
        return(pageNotFound(str(e)))


@app.route("/postalentry")
def postalentry():
    tickets = loginCheck()
    if g.postalVote and g.user:
        print("postalEntry - " + g.postalVote)
        raffleName, sizeVariant = g.postalVote.split("---")
        verificationCode = g.user + "---" + g.postalVote
        entryArray = ["NULL", g.user, verificationCode, 0]
        addEntry(databaseName, "postalVotes", entryArray)
        encodeQR(urlPrefix + "/postalconfirm/" + verificationCode, verificationCode)
        session["postalVote"] = None
        print("postalEntry - Cleared Postal Session")
        return render_template("postalentry.html", tickets=tickets, title="Postal Entry", QRCodeLocation=url_for("static", filename="QRCodes/"+verificationCode+".png"), raffleName=raffleName, sizeVariant=sizeVariant)
    else:
        print("postalEntry - none")
        return(redirect(url_for("login")))
    return(redirect(url_for("notFound")))


@app.route("/postalconfirm/<verificationCode>")
def postalconfirm(verificationCode):
    tickets = loginCheck()
    if isAdmin():
        message = "Sucessfully added user to raffle"
        rows = list(searchTableColumnEntry(databaseName, "postalVotes", "verificationCode", verificationCode))[0]
        return render_template("message.html", tickets=tickets, title="Success", message=["Success", message], loggedIn=True)


@app.route("/about")
def about():
    tickets = loginCheck()
    return render_template("about.html", tickets=tickets, title="About")


@app.route("/topup")
def topup():
    tickets = loginCheck()
    return render_template("topup.html", tickets=tickets, title="Top Up")


@app.route("/purchase")
def purchase():
    tickets = loginCheck()
    return render_template("purchase.html", tickets=tickets, title="Top Up")


@app.route("/add500")
def add500():
    tickets = int(loginCheck())+500
    if g.user:
        worked2 = updateEntry(databaseName, "users", "username", g.user, "tickets", str(int(tickets)))
        message = str(500) + " tickets added to " + g.user + str(worked2)
    else:
        message = str(500) + "failed to add 500 tickets"
        
    return render_template("message.html", tickets=tickets, title="addtix500", message=["Add 500 Tix",message])


@app.route("/faq")
def faq():
    tickets = loginCheck()
    return render_template("faq.html", tickets=tickets, sections=faqs, title="FAQs")


@app.route("/suggestions")
def suggestions():
    tickets = loginCheck()
    return render_template("suggestions.html", tickets=tickets, title="Suggest A Product")


@app.route("/memberzone", methods=["GET", "POST"])
def memberzone():
    tickets = loginCheck()
    message = False
    if g.user:
        rows = searchTableColumnEntry(databaseName, "users", "username", g.user)
        tickets = g.tickets
        try:
            rows = list(rows[0])
            if rows[20] != "NULL":
                endEpoch = float(rows[20])
                print("exclude - endEpoch: " + str(endEpoch))
                print("exclude - time.time(): " + str(time.time()))
                if time.time() < endEpoch:
                    endDate = time.strftime('%d/%m/%Y at %H:%M:%S', time.localtime(endEpoch))
                    print("exclude - endDate: " + str(endDate))
                    message = "You chose to exclude yourself until " + str(endDate) + ". Our staff won't unblock your account before then"
        except:
            rows = []
        try:
            raffles = rows[21].split(", ") ####################################################################### this bit
        except:
            pass
        returnRaffles = []
        for raffle in range(len(raffles)):
            try:
                raffles[raffle] = raffles[raffle].split(" -#- ")
                print(raffles[raffle])
                print(int(raffles[raffle][0]))
                rowsItem = list(searchTableColumnEntry(databaseName, "items", "id", int(raffles[raffle][0]))[0])
                print(rowsItem)
                returnRaffles.append([rowsItem[1], rowsItem[8], rowsItem[3], rowsItem[7], time.strftime('%d/%m/%Y', time.localtime(int(raffles[raffle][1]))), rowsItem[2]])
            except:
                pass
            

        return render_template("memberzone.html", tickets=tickets, title="Member Zone", rows=rows, raffles=returnRaffles)
    else:
        return(login())

@app.route("/memberzoneold", methods=["GET", "POST"])
def memberzoneold():
    tickets = loginCheck()
    message = False
    if g.user:
        rows = searchTableColumnEntry(databaseName, "users", "username", g.user)
        tickets = g.tickets
        try:
            rows = list(rows[0])
            if rows[20] != "NULL":
                endEpoch = float(rows[20])
                print("exclude - endEpoch: " + str(endEpoch))
                print("exclude - time.time(): " + str(time.time()))
                if time.time() < endEpoch:
                    endDate = time.strftime('%d/%m/%Y at %H:%M:%S', time.localtime(endEpoch))
                    print("exclude - endDate: " + str(endDate))
                    message = "You chose to exclude yourself until " + str(endDate) + ". Our staff won't unblock your account before then"
        except:
            rows = []
        try:
            raffles = rows[21].split(", ") ####################################################################### this bit
        except:
            pass
        returnRaffles = []
        for raffle in range(len(raffles)):
            try:
                raffles[raffle] = raffles[raffle].split(" -#- ")
                print(raffles[raffle])
                print(int(raffles[raffle][0]))
                rowsItem = list(searchTableColumnEntry(databaseName, "items", "id", int(raffles[raffle][0]))[0])
                print(rowsItem)
                returnRaffles.append([rowsItem[1], rowsItem[8], rowsItem[3], rowsItem[7], time.strftime('%d/%m/%Y', time.localtime(int(raffles[raffle][1]))), rowsItem[2]])
            except:
                pass
            

        return render_template("memberzone.old.html", tickets=tickets, title="Member Zone", rows=rows, raffles=returnRaffles)
    else:
        return(login())


#############################################
@app.route("/login", methods=["POST", "GET"])
def login():
    tickets = loginCheck()
    if g.user:
        return redirect("/memberzone")
    if request.method == "POST":
        print("one")
        
        username = request.form["username"]
        password = request.form["password"]
        values = [username, password]
        prompts = [False, False]
        cssClassSignin = ["login-text", "login-text"]
        
        if len(username) > 200:
            print("two")
            cssClassSignin[0] = "login-text-error"
            prompts[0] = "Your username can't be longer than 200 characters"
        if len(password) > 200:
            print("three")
            cssClassSignin[1] = "login-text-error"
            prompts[1] = "Your username can't be longer than 200 characters"
        if prompts[0] != False and prompts[1] != False:
            print("four")
            return render_template("login.html", tickets=tickets, title="Login", values=values, prompts=prompts, cssClassSignin=cssClassSignin)

        # check database for username
        if len(username) == 0:
            print("five")
            prompts[0] = "Username can't be blank"
            cssClassSignin[0] = "login-text-error"
        rowsUsername = searchTableColumnEntry(databaseName, "users", "username", username)
        if str(rowsUsername) != "[]" and rowsUsername != False:
            print("six")
            print(str(rowsUsername))
            prompts[0] = "Account exists"
            cssClassSignin[0] = "signup-text-ok"
        try:
            print("seven")
            if rowsUsername == [] or list(rowsUsername[0][1]) != username:
                print("eight")
                prompts[0] = "Account does not exist, this is case sensative"
                cssClassSignin[0] = "login-text-error"
        except:
                print("nine")
                prompts[0] = "Account does not exist, this is case sensative"
                cssClassSignin[0] = "login-text-error"

        try: # compare password to stored password
            print("ten")
            array = list(rowsUsername[0])
            salt = array[4]
            hashedPassword = str(passwordHash(salt, password))
            rowsUsername = searchTableColumnEntry(databaseName, "users", "username", username)
            worked2 = updateEntry(databaseName, "users", "username", username, "passwordFix", hashedPassword)
            rowsUsername2 = searchTableColumnEntry(databaseName, "users", "username", username)
            print("\n   After Update " + str(rowsUsername2))
            array = list(rowsUsername2[0])
            if array[1] == username and array[3] == array[6]:
                print("eleven")
                session.pop("user", None)
                session.pop("tickets", None)
                session.pop("exclude", None)
                print("\n\n\n")
                session["user"] = username
                session["tickets"] = array[15]
                if array[20] != "NULL":
                    print("twelve")
                    session["exclude"] = array[20]
                print("login - user: " + str(g.user))
                print("login - array tickets: " + str(array[15]))
                print("login - tickets: " + str(g.tickets))
                print("\n\n\n")
                return redirect("/memberzone")
            else:
                print("thirteen")
                prompts[1] = "Incorrect Password"
                cssClassSignin[1] = "login-text-error"
        except Exception as e:
            print("login - FAILED: " + str(e))

        
        if len(password) == 0:
            prompts[1] = "Password can't be blank"
            cssClassSignin[1] = "login-text-error"
        
    elif request.method == "GET":
        values = ["", ""]
        prompts = ["", ""]
        cssClassSignin = ["login-text", "login-text"]
        return render_template("login.html", tickets=tickets, title="Login", values=values, prompts=prompts, cssClassSignin=cssClassSignin)
    else:
        return render_template("login.html", tickets=tickets, title="Login", values=values, prompts=prompts, cssClassSignin=cssClassSignin)
    return render_template("login.html", tickets=tickets, title="Login", values=values, prompts=prompts, cssClassSignin=cssClassSignin)
#############################################

@app.route("/logout", methods=["POST", "GET"])
def logout():
    print("\n\n\nLogging the user out")
    try:
        print("Logging the user out with session.clear()\n\n\n")
        session.clear()
    except:
        pass
    tickets = loginCheck()
    
    return redirect("/login")


@app.route("/signup.html")
def signupRedir():
    return redirect(url_for("signup"))


@app.route("/signup", methods=["POST", "GET"])
def signup():
    print("\n########## - signup start")
    tickets = loginCheck()
    if g.user:
        return redirect("/memberzone")
    values = ["", "", "", "", "", "", "", ""]
    prompts = ["", "", "", "", "", "", "", ""]
    cssClassSignup = ["form-control", "form-control", "form-control", "form-control", "form-control", "form-control", "form-control", "form-control"]
    print("\n########## - signup 1")
    marketing = "No"
    if request.method == "POST":
        print("\n########## - signup 2")
        username = request.form["username"]
        email = request.form["email"]
        password1 = request.form["password1"]
        password2 = request.form["password2"]
        firstname = request.form["firstname"]
        surname = request.form["surname"]
        number = request.form["number"]
        dob = request.form["dob"]
        marketing = request.form["marketing"]
        terms = request.form["terms"]
        data = [username, email, password1, password2, firstname, surname, dob, number, marketing]
        fieldNames = ["username", "email", "password", "password", "firstname", "surname", "date of birth", "number", "marketing"]
        values = data
        print("\n values: " + str(values))
        prompts = [False, False, False, False, False, False, False, False, False]

        # make sure entries are not blank
        for item in range(len(data)):
            if len(data[item]) == 0:
                cssClassSignup[item] = "form-control form-control-error"
                prompts[item] = "Your " + fieldNames[item] + " can't be blank"
            if len(data[item]) > 200:
                cssClassSignup[item] = "form-control form-control-error"
                prompts[item] = "Your " + fieldNames[item] + " can't be longer than 200 characters"

        # validate username
        print("\n########## - signup username")
        
        rows = searchTableColumnEntry(databaseName, "users", "username", username)
        if rows != False and len(rows) != 0:
            prompts[0] = "Username isn't avaliable"
            cssClassSignup[0] = "form-control form-control-error"

        # validate email
        print("\n########## - signup email")
        if validateEmail(email):
            prompts[1] = validateEmail(email)
            cssClassSignup[1] = "form-control form-control-error"
        rows = searchTableColumnEntry(databaseName, "users", "email", email)
        if rows != False and len(rows) != 0:
            prompts[1] = "This email address is already in use"
            cssClassSignup[1] = "form-control form-control-error"


        # validate password
        print("\n########## - signup password")
        if len(password1) < 12 or password1 != password2:
            prompts[2] = "Passwords must match and be 12-64 characters long"
            cssClassSignup[2] = "form-control form-control-error"
            cssClassSignup[3] = "form-control form-control-error"
            # pop one list from the other you nonce
            
            
        #validate date of birth
        print("\n########## - signup dob")
        try:
            day = int(dob[0:2])
            month = int(dob[3:5])
            year = int(dob[6:10])
            if year < 1875: # derived from the oldest person on earth in 2020 + a decent margin to avoid accidental discrimination 
                0/0
            if month <1 or month > 12:
                0/0
            if day <1 or day > 31:
                0/0
            birth_date = datetime.date(year, month, day)
            end_date = datetime.date.today()
            time_diff = end_date - birth_date
            days_diff = int(str(time_diff).split(",")[0].split(" ")[0])

            if days_diff < 6570:
                return render_template("message.html", tickets=tickets, title="Age Restricted", message=["Age Restricted", "I'm sorry, you're not old enough to use our site. \nFeel free to come back when you're 18"])


        except:
            cssClassSignup[6] = "form-control form-control-error"
            if len(dob) != 0:
                prompts[6] = "Date of Birth format is DD/MM/YYYY"
        

        print("\n########## - signup password 2")

        # validate password
        if len(password1) < 12 or password1 != password2:
            prompts[2] = "Passwords must match and be 12-64 characters long"
            cssClassSignup[2] = "form-control form-control-error"
            cssClassSignup[3] = "form-control form-control-error"
            # pop one list from the other you nonce
        
        # validate terms and conditions
        # if terms
        detailsValid = True
        for detail in prompts:
            if detail != False:
                detailsValid = False
                print("\n########## - signup details invalid")
                break

        print("\n########## - signup details valid")
        if detailsValid == True:
            timeJoined = str(time.time())
            salt = str(quickHash(str(timeJoined), username+str(timeJoined)))
            userEntry = ["NULL", username, email, "XCFplaceXCFholderXCF", salt, str(timeJoined), "XCFplaceXCFholderXCF", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", 0, number, firstname, surname, "NULL", "NULL", "NULL", dob, marketing]
            worked1 = addEntry(databaseName, "users", userEntry)
            print("##### signup - " + str(worked1))
            worked2 = False
            
            try:
                rowsPasswordFix = searchTableColumnEntry(databaseName, "users", "username", username)
                arrayPasswordFix = list(rowsPasswordFix[0])
                # saltPasswordFix = arrayPasswordFix[4]
                hashedPassword = str(passwordHash(salt, password1))
                print("signup - rowsPasswordFix: " + str(rowsPasswordFix))
                worked2 = updateEntry(databaseName, "users", "username", username, "hash", hashedPassword)
                rowsPasswordFix = searchTableColumnEntry(databaseName, "users", "username", username)
                print(str(rowsPasswordFix))
            except Exception as e:
                message = "signupPost1 - " + str(e)
                return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])

            if worked1 and worked2:
                session.pop("user", None)
                session.pop("tickets", None)
                session["user"] = username
                session["tickets"] = 0
                if g.user:
                    print("signup - g.user: " + str(g.user))
                if g.tickets:
                    print("signup - g.tickets: " + str(g.tickets))
                message = "Welcome to RaffleSauce " + username
                return render_template("message.html", tickets=tickets, title="Success", message=["Success", message], loggedIn=True, animation=True)
            else:
                message = "Something went wrong - signupPost2"
                return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])

        else:
            return render_template("signup.html", tickets=tickets, title="Sign Up", cssClassSignup=cssClassSignup, unPlaceholder="Username", values=values, prompts=prompts)
    elif request.method == "GET":
        return render_template("signup.html", tickets=tickets, title="Sign Up", cssClassSignup=cssClassSignup, unPlaceholder="Username", values=values, prompts=prompts)


@app.route("/changedetails", methods=["POST", "GET"])
def changedetails():
    print("\n########## - changedetails start")
    tickets = loginCheck()
    if not g.user:
        return redirect("/login")
    prompts = ["", "", "", "", "", "", "", "", ""]
    cssClassSignup = ["form-control", "form-control", "form-control", "form-control", "form-control", "form-control", "form-control", "form-control", "form-control"]
    print("\n########## - changedetails 1")
    try:
        userData = list(searchTableColumnEntry(databaseName, "users", "username", g.user)[0])
        values = ["", userData[2], "", "", userData[17], userData[18], userData[22], userData[16], ""]
    except:
        values = ["", "", "", "", "", "", "", "", ""]
    marketing = "No"
    if request.method == "POST":
        print("\n########## - changedetails 2")
        username = request.form["username"]
        email = request.form["email"]
        password1 = request.form["password1"]
        password2 = request.form["password2"]
        firstname = request.form["firstname"]
        surname = request.form["surname"]
        number = request.form["number"]
        dob = request.form["dob"]
        data = [username, email, password1, password2, firstname, surname, dob, number]
        fieldNames = ["username", "email", "password", "password", "firstname", "surname", "date of birth", "number"]
        values = data
        print("\n values: " + str(values))
        prompts = [False, False, False, False, False, False, False, False, False]

        # make sure entries are not blank
        for item in range(len(data)):
            if len(data[item]) == 0:
                cssClassSignup[item] = "form-control form-control-error"
                prompts[item] = "Your " + fieldNames[item] + " can't be blank"
            if len(data[item]) > 200:
                cssClassSignup[item] = "form-control form-control-error"
                prompts[item] = "Your " + fieldNames[item] + " can't be longer than 200 characters"

        # validate username
        print("\n########## - changedetails username")
        
        rows = searchTableColumnEntry(databaseName, "users", "username", username)
        if rows != False and len(rows) != 0:
            prompts[0] = "Username isn't avaliable"
            cssClassSignup[0] = "form-control form-control-error"

        # validate email
        print("\n########## - changedetails email")
        if validateEmail(email):
            prompts[1] = validateEmail(email)
            cssClassSignup[1] = "form-control form-control-error"
        rows = searchTableColumnEntry(databaseName, "users", "email", email)
        if rows != False and len(rows) != 0:
            prompts[1] = "This email address is already in use"
            cssClassSignup[1] = "form-control form-control-error"


        # validate password
        print("\n########## - changedetails password")
        if len(password1) < 12 or password1 != password2:
            prompts[2] = "Passwords must match and be 12-64 characters long"
            cssClassSignup[2] = "form-control form-control-error"
            cssClassSignup[3] = "form-control form-control-error"
            # pop one list from the other you nonce
            
            
        #validate date of birth
        print("\n########## - changedetails dob")
        try:
            day = int(dob[0:2])
            month = int(dob[3:5])
            year = int(dob[6:10])
            if year < 1875: # derived from the oldest person on earth in 2020 + a decent margin to avoid accidental discrimination 
                0/0
            if month <1 or month > 12:
                0/0
            if day <1 or day > 31:
                0/0
            birth_date = datetime.date(year, month, day)
            end_date = datetime.date.today()
            time_diff = end_date - birth_date
            days_diff = int(str(time_diff).split(",")[0].split(" ")[0])

            if days_diff < 6570:
                return render_template("message.html", tickets=tickets, title="Age Restricted", message=["Age Restricted", "I'm sorry, you're not old enough to use our site. \nFeel free to come back when you're 18"])


        except:
            cssClassSignup[6] = "form-control form-control-error"
            if len(dob) != 0:
                prompts[6] = "Date of Birth format is DD/MM/YYYY"
        

        print("\n########## - signup password 2")

        # validate password
        if len(password1) < 12 or password1 != password2:
            prompts[2] = "Passwords must match and be 12-64 characters long"
            cssClassSignup[2] = "form-control form-control-error"
            cssClassSignup[3] = "form-control form-control-error"
            # pop one list from the other you nonce
        
        detailsValid = True
        for detail in prompts:
            if detail != False:
                detailsValid = False
                print("\n########## - changedetails details invalid")
                break

        print("\n########## - changedetails details valid")
        if detailsValid == True:
            timeJoined = str(time.time())
            salt = str(quickHash(str(timeJoined), username+str(timeJoined)))
            userEntry = ["NULL", username, email, "XCFplaceXCFholderXCF", salt, str(timeJoined), "XCFplaceXCFholderXCF", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", 0, number, firstname, surname, "NULL", "NULL", "NULL", dob, marketing]
            worked1 = addEntry(databaseName, "users", userEntry)
            print("##### changedetails - " + str(worked1))
            worked2 = False
            
            try:
                rowsPasswordFix = searchTableColumnEntry(databaseName, "users", "username", username)
                arrayPasswordFix = list(rowsPasswordFix[0])
                # saltPasswordFix = arrayPasswordFix[4]
                hashedPassword = str(passwordHash(salt, password1))
                print("changedetails - rowsPasswordFix: " + str(rowsPasswordFix))
                worked2 = updateEntry(databaseName, "users", "username", username, "hash", hashedPassword)
                rowsPasswordFix = searchTableColumnEntry(databaseName, "users", "username", username)
                print(str(rowsPasswordFix))
            except Exception as e:
                message = "changedetailsPost1 - " + str(e)
                return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])

            if worked1 and worked2:
                session.pop("user", None)
                session.pop("tickets", None)
                session["user"] = username
                session["tickets"] = 0
                if g.user:
                    print("changedetails - g.user: " + str(g.user))
                if g.tickets:
                    print("changedetails - g.tickets: " + str(g.tickets))
                message = "Welcome to RaffleSauce " + username
                return render_template("message.html", tickets=tickets, title="Success", message=["Success", message], loggedIn=True)
            else:
                message = "Something went wrong - changedetailsPost2"
                return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])

        else:
            return render_template("changedetails.html", tickets=tickets, title="Change Details", cssClassSignup=cssClassSignup, unPlaceholder="Username", values=values, prompts=prompts)
    elif request.method == "GET":
        try:
            print(g.user)
            print(g.user)
            print(g.user)
            print(g.user)
            print(g.user)
            print(g.user)
            print(g.user)
            print(g.user)
            print(g.user)
            userData = list(searchTableColumnEntry(databaseName, "users", username, g.user))[0]
        except Exception as e:
            pass
        return render_template("changedetails.html", tickets=tickets, title="Change Details", cssClassSignup=cssClassSignup, unPlaceholder="Username", values=values, prompts=prompts)


def fixString(string):
    invalid = ['<', '>', ':', '"', '|', '*', '/', '\\', '?']
    invalidStrings = ["con", "prn", "aux", "nul", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9", "COM0", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9", "LPT0"]
    for invalidStr in invalidStrings:
        if invalidStr.lower() == string.lower():
            return("NotAllowed")
    string = list(string)
    returnString = ""
    for character in string:
        ok = True
        for invalidChar in invalid:
            if character == invalidChar:
                ok = False
                break
        if ok == True:
            returnString += character
    if len(returnString) == 0:
        returnString = "NoNameSpecifiedOrAllCharactersAreIllegal"
    return(returnString)


@app.route("/createraffle", methods=["GET", "POST"])
def createraffle():
    tickets = loginCheck()
    if g.user:
        if isAdmin():
            print("createraffle - g.user: " + str(g.user))
            return render_template("createraffle.html", tickets=tickets, title="Create Raffle")
        else:
            return(notFound())
    return(notFound())


@app.route("/uploadimage", methods=["GET", "POST"])
def uploadimage():
    tickets = loginCheck()
    allDataCorrect = True
    if request.method == "POST" and (isAdmin()):

        if request.files:
        # if request.files and request.itemName:

            images = request.files.getlist("files")
            itemNameUnsafe = str(request.form.get("itemName"))
            itemDropDate = str(request.form.get("itemDropDate"))
            itemTicketCost = str(request.form.get("ticketCost"))
            itemDescription = str(request.form.get("itemDescription"))
            itemRealWorldCost = str(request.form.get("itemValue"))
            itemVariant = str(request.form.get("itemVariant"))
            itemVariants = str(request.form.get("itemVariants"))
            dateCreated = str(request.form.get("created"))

            errorMessage = "Something went wrong"
            



            if dateCreated == "":
                dateCreated = int(time.time())
            itemName = fixString(itemNameUnsafe)
            if itemName == "NoNameSpecifiedOrAllCharactersAreIllegal":
                errorMessage = "The item name must not be blank"
                allDataCorrect = False

            try:
                search = list(searchTableColumnEntry(databaseName, "items", "name", itemName)[0])
                alreadyExists = True
            except:
                alreadyExists = False

            pattern = '%d/%m/%Y %H:%M:%S'
            try:
                itemDropDate = int(time.mktime(time.strptime(itemDropDate, pattern)))
            except:
                errorMessage = "Drop date must be in format 'DD/MM/YYYY HH:MM:SS'"
                allDataCorrect = False
            try:
                tixCostTest = int(itemTicketCost)
            except:
                errorMessage = "Ticket Cost must be a whole number > 0"
                allDataCorrect = False

            message = itemName + " page created"
            try:
                totalImages = len(images)
                if totalImages < 2:
                    errorMessage = "It seems you've uploaded too few images, make sure you select both images upon upload and they contain left and right in their respective names"
                    allDataCorrect = False
                    0/0
                try:
                    os.mkdir("static/raffles/" + removeSpace(itemName))
                except:
                    pass
                itera = 0
                location = "static/raffles/" + removeSpace(itemName) + "/"
                
                for image in images:
                    imageName = str(image).split("'")[1]
                    print(imageName)
                    if "left" in imageName or "Left" in imageName or "LEFT" in imageName:
                        image.save(location + "image1.png")
                        print("uploadimage - saved as image 1")
                    elif "right" in imageName or "Right" in imageName or "RIGHT" in imageName:
                        image.save(location + "image0.png")
                        print("uploadimage - saved as image 0")
                    else:
                        image.save(location + "image" + str(itera) + ".png")
                        print("uploadimage - saved as itera name")
                    itera += 1
                itemEntry = ["NULL", itemName, itemDropDate, itemTicketCost, itemDescription, itemVariant, itemVariants, location, itemRealWorldCost, dateCreated]





                





                if alreadyExists == False and allDataCorrect:
                    # create entry in items table
                    worked = addEntry(databaseName, "items", itemEntry)
                    # create table for raffle entries
                    raffleItem = [["id", "integer", "primary key", "autoincrement"], ["username", "text"], ["epochDateEntered", "integer"], ["won", "integer"], ["paid", "integer"]]
                    worked2 = safeCreateTable(databaseName, removeSpace(itemName), raffleItem)
                    raffleEntries = [["id", "integer", "primary key", "autoincrement"], ["userID", "int"], ["postal", "integer"], ["postalValid", "integer"], ["won", "integer"], ["time", "text"], ["variant", "text"], ["username", "text"]]
                    worked3 = safeCreateTableFK(databaseName, removeSpace(itemName)+"Entries", raffleEntries, "userID", "users", "id")
                    message = "Created page " + urlPrefix + "/raffle/" + removeSpace(itemName)
                    worked3 = True
                elif alreadyExists and allDataCorrect:
                    worked = updateEntry(databaseName, "items", "name", itemName, "dropdate", itemDropDate)
                    worked = updateEntry(databaseName, "items", "name", itemName, "ticketCost", itemTicketCost)
                    worked = updateEntry(databaseName, "items", "name", itemName, "description", itemDescription)
                    worked = updateEntry(databaseName, "items", "name", itemName, "variant", itemVariant)
                    worked = updateEntry(databaseName, "items", "name", itemName, "variants", itemVariants)
                    worked = updateEntry(databaseName, "items", "name", itemName, "location", location)
                    worked = updateEntry(databaseName, "items", "name", itemName, "realWorldCost", itemRealWorldCost)
                    worked = updateEntry(databaseName, "items", "name", itemName, "dateCreated", dateCreated)
                    worked2 = True
                    message = "Updated page " + urlPrefix + "/raffle/" + removeSpace(itemName)
                    

                if allDataCorrect == True and worked == True and worked2 == True and worked3 == True:
                    print("allDataCorrect plus worked 1,2 and 3 are all True")
                    return render_template("message.html", tickets=tickets, title="Success", message=["Success", message])
                else:
                    0/0
                return render_template("message.html", tickets=tickets, title="Success", message=["Success", message])
            except:
                pass
            message = "Something may have gone wrong - " + message


            if allDataCorrect == False:
                return render_template("createraffle.html", tickets=tickets, title="Create Raffle", errorMessage=errorMessage)


            return render_template("message.html", tickets=tickets, title="Success", message=["Success", message])
    else:
        pass
    try:
        message = "uploadImage - Something went wrong, probably not logged in - " + message
    except:
        message = "uploadImage - Something went wrong, probably not logged in"
    if allDataCorrect == False:
        errorMessage = "Something went wrong, please try again."
        return render_template("createraffle.html", tickets=tickets, title="Create Raffle", errorMessage=errorMessage)
    return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])


@app.route("/checkout", methods=["POST", "GET"])
def checkout():
    tickets = loginCheck()
    checker, excludeReturn = excludeCheck(tickets)
    if checker == True:
        return(excludeReturn)
    if g.user:
        pass
    else:
        return redirect("/login")

    if request.method == "POST":
        return render_template("checkout.html", tickets=tickets, title="Secure Checkout")
    elif request.method == "GET":
        return render_template("checkout.html", tickets=tickets, title="Secure Checkout")
    else:
        return render_template("checkout.html", tickets=tickets, title="Secure Checkout")


@app.route("/checkout/<amount>", methods=["POST", "GET"])
def dynamiccheckout(amount=1):
    tickets = loginCheck()
    checker, excludeReturn = excludeCheck(tickets)
    if checker == True:
        return(excludeReturn)
    if g.user:
        pass
    else:
        return redirect("/login")
    names = ["Title", "Card Surname", "Address Line 1", "Address Line 2", "Address Line 3", "Town/City", "Post Code", "Country"]
    prompts = ["", "", "", "", "", "", "", "", ""]
    cssClassCheckout = ["form-control", "form-control", "form-control", "form-control", "form-control", "form-control", "form-control", "form-control", "form-control"]
    values = ["", "", "", "", "", "", "", ""]
    countries = pycountry.countries
    # print("\n\n\nGot to here\n\n\n")
    if request.method == "GET":
        rowsUsername = list(searchTableColumnEntry(databaseName, "users", "username", g.user)[0])
        print("\n\n\n")
        print("dynamiccheckout - rowsUsername: " + str(rowsUsername))
        print("\n\n\n")
        if rowsUsername[0] != "NULL" and rowsUsername[7] != "NULL":
            values = [rowsUsername[7], rowsUsername[8], rowsUsername[9], rowsUsername[10], rowsUsername[11], rowsUsername[12], rowsUsername[13], rowsUsername[14]]
            prompts[8] = "Make sure all your details are correct before continuing"
        return render_template("checkout.html", tickets=tickets, prompts=prompts, values=values, cssClassCheckout=cssClassCheckout, countries=countries, title="Secure Checkout")

    title = request.form["customer_title"]
    surname = request.form["customer_surname"]
    adLn1 = request.form["customer_address_line1"]
    adLn2 = request.form["customer_address_line2"]
    adLn3 = request.form["customer_address_line3"]
    town = request.form["customer_address_town"]
    postcode = request.form["customer_address_postcode"]
    country = request.form["customer_address_country"]
    values = [title, surname, adLn1, adLn2, adLn3, town, postcode, country]
    try:
        amount = int(amount)
    except:
        message = "checkout - '" + str(amount) + "' is an invalid amount of tickets. Enter a whole number"
        return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])
    if amount < 1:
        message = "checkout - '" + str(amount) + "' is an invalid amount of tickets. Enter a whole number above 0"
        return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])
    
    print("\n\n\n")
    print("dynamiccheckout - " + str(values))
    print("\n\n\n")
    detailsValid = True
    for value in range(len(values)):
        if (len(values[value]) == 0 or values[value] == "Select Country") and value != 3 and value != 4:
            prompts[value] = names[value] + " can't be blank"
            cssClassCheckout[value] = "form-control"
            detailsValid = False
    if detailsValid == False:
        return render_template("checkout.html", tickets=tickets, prompts=prompts, values=values, cssClassCheckout=cssClassCheckout, countries=countries, title="Secure Checkout")
    else:
        userData = [g.user]
        tableName = "users"
        colNames = ["customer_title", "customer_surname", "customer_address_line1", "customer_address_line2", "customer_address_line3", "customer_address_town", "customer_address_postcode", "customer_address_country"]
        try:
            for index in range(len(colNames)):
                updateEntry(databaseName, tableName, "username", g.user, colNames[index], values[index])
        except Exception as e:
            print("\n\n\ndynamiccheckout - " + str(e) + "\n\n\n")
        return dynamiccheckoutpayment(amount=amount, checkoutData=values, userData=userData, names=names)


@app.route("/checkoutpayment/<amount>")
def dynamiccheckoutpayment(amount=1, checkoutData=None, userData=None, names=None):
    tickets = loginCheck()
    checker, excludeReturn = excludeCheck(tickets)
    if checker == True:
        return(excludeReturn)
    try:
        amount = int(amount)
    except:
        message = "checkout - '" + str(amount) + "' is an invalid amount of tickets. Enter a whole number"
        return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])
    if amount < 1:
        message = "checkout - '" + str(amount) + "' is an invalid amount of tickets. Enter a whole number above 0"
        return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])
    url = 'https://demo.charitypayments.co.uk/hosted/payment'
    item = str(amount) + " Ticket"
    if amount > 1:
        item += "s"
    try:
        username = g.user
        epochDateSent = time.time()*10000000
        hashed = quickHash(str(epochDateSent) + ", " + username, str(epochDateSent))
        entryArray = ["NULL", username, hashed, amount, amount, str(epochDateSent), "NULL", 0]
        worked = addEntry(databaseName, "payments", entryArray)
        rows = searchTableColumnEntry(databaseName, "payments", "epochDateSent", str(epochDateSent))
        hashed = list(rows[0])[2]
        returnURL = str(get('https://api.ipify.org').text)+":5000/paymentsuccess/" + hashed + "5mERikMs1" + str(amount)


        if worked == True:
            myobj = {
                "payment_amount": amount,
                "return_url": returnURL,
                "customer_email": list(rows[0])[2],
                "customer_title": checkoutData[0],
                "customer_surname": checkoutData[1],
                "customer_address_line1": checkoutData[2],
                "customer_address_line2": checkoutData[3],
                "customer_address_line3": checkoutData[4],
                "customer_address_town": checkoutData[5],
                "customer_address_postcode": checkoutData[6],
                "customer_address_country": checkoutData[7],
                "basket_items[1][label]": item,
                "basket_items[1][amount]": amount
            }
            postRequest = requests.post(url, data = myobj)
            page = postRequest.text
            soup = BeautifulSoup(page)
            head = soup.find('head')
            head = head.findChildren(recursive=False)[0]
            body = soup.find('body')
            body = body.findChildren(recursive=False)[0]
        else:
            message = "Something went wrong. You have not been charged."
            return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])
    except:
        return redirect("/purchase")
    return render_template("checkoutpayment.html", tickets=tickets, title="Secure Checkout", gatewayBody=body, gatewayHead=head, checkoutData=checkoutData, item=item)


@app.route("/paymentsuccess/<verificationCode>")
def paymentsuccess(verificationCode):
    tickets = loginCheck()
    try:
        array = verificationCode.split("5mERikMs1")
        code = array[0]
        amount = int(array[1])
    except:
        message = "Something went wrong. If you dont have your tickets and the money left your account, get in contact with our tech team via discord, instagram or the email button at the bottom of the page"
        return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])
    suffix = ""
    if amount > 1:
        suffix = "s"

    # Process deals
    #################################################################################################################################################################################################### - make this more accurate
    if amount == 45:
        amount = 50
    elif amount == 20:
        amount = 22
    elif amount == 10:
        amount = 11
    elif amount > 45:
        if amount > 100:
            amount = int(round((amount/45)*50))-1
        else:
            amount = int(round((amount/45)*50))
    elif amount > 20:
        amount = int(round((amount/20)*22))
    elif amount > 10:
        amount = int(round((amount/10)*11))
    #################################################################################################################################################################################################### - make this more accurate
    try:
        rowsPayments = list(searchTableColumnEntry(databaseName, "payments", "verificationCode", code)[0])
        usernameAdded = rowsPayments[1]
        if rowsPayments[7] == 0:
            colNames = ["status", "epochDateComplete"]
            values = [1, time.time()*10000000]
            for index in range(len(colNames)):
                updateEntry(databaseName, "payments", "verificationCode", code, colNames[index], values[index])
            rowsUsers = list(searchTableColumnEntry(databaseName, "users", "username", usernameAdded)[0])
            newTickets = int(rowsUsers[15]) + amount
            updateEntry(databaseName, "users", "username", usernameAdded, "tickets", newTickets)
            session["tickets"] = newTickets
            tickets = newTickets
            message = "Added "+ str(amount)+" ticket" + suffix + " to " + usernameAdded + ". Thank you for your purchase"
        else:
            try:
                humanReadable = time.strftime('%d/%m/%Y %H:%M:%S', time.localtime(int(rowsPayments[6])/10000000))
            except Exception as e:
                print("paymentsuccess 1st - " + str(e))
                message = "Something went wrong converting time in payment process"
                return render_template("message.html", tickets=tickets, title="Failure", message=["Failure", message])
            message = str(amount)+" ticket" + suffix + " were added to '" + usernameAdded + "' on '" + humanReadable + "'"
    except Exception as e:
        print("paymentsuccess 2nd - " + str(e))
        message = "If you didnt recieve your tickets you should get in touch with our tech team."
        return render_template("message.html", tickets=newTickets, title="Failure", message=["Failure", message])
    return render_template("message.html", tickets=tickets, title="Success", message=["Success", message])


@app.route("/exclude", methods=["GET", "POST"])
def exclude():
    tickets = loginCheck()
    checker, excludeReturn = excludeCheck(tickets)
    if checker == True:
        return(excludeReturn)

    print("\n\n\n")
    

    if request.method == "POST":
        print("exclude - method: POST")

        try:
            duration = int(request.form["duration"].split(" ")[0])
            print("exclude - duration: " + str(duration))
            updateEntry(databaseName, "users", "username", g.user, "excludeuntil", time.time()+(86400*duration))
        except Exception as e:
            print("\n\n\nexclude error X02548")
            print(str(e))
            if g.user:
                return(redirect(url_for("memberzone")))
            else:
                return(redirect(url_for("login")))
                
    elif request.method == "GET":
        print("exclude - method: GET")
    else:
        print("exclude - method: " + str(request.method))
        
    print("\n\n\n")
    return(redirect(url_for("memberzone")))


@app.route("/meme", methods=["GET", "POST"])
def meme():
    return render_template("meme.html")
    

@app.before_request
def before_request():
    g.user = None
    g.tickets = None
    g.exclude = None
    g.postalVote = None

    if "user" in session:
        g.user = session["user"]
    
    if "tickets" in session:
        g.tickets = session["tickets"]
    
    if "exclude" in session:
        g.exclude = session["exclude"]
    
    if "postalVote" in session:
        g.postalVote = session["postalVote"]


if __name__ == "__main__":
    # app.run(host="192.168.1.177", port=5001, debug=True)
    app.run(host="localhost", port=5001, debug=True)